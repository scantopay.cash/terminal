import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
  // /
  {
    path: '/',
    component: () => import('pages/Home.vue')
  },
  // /setup/xxx
  {
    path: '/setup/:terminalId',
    component: () => import('pages/Setup.vue')
  },
  // /terminal
  {
    path: '/t/:terminalId/:terminalPassword',
    component: () => import('pages/terminal/_Layout.vue'),
    children: [
      { path: '', redirect: to => `${to.path}/checkout` },
      { path: 'checkout', component: () => import('pages/terminal/Checkout.vue') },
      { path: 'settings', component: () => import('pages/terminal/Settings.vue') },
      { path: 'payment-qr', component: () => import('pages/terminal/PaymentQR.vue') },
      { path: 'extras', component: () => import('pages/terminal/Extras.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
