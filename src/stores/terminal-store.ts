import { defineStore } from 'pinia'

import { Settings } from '../interfaces'

import { Loading, Notify } from 'quasar'
import axios from 'axios'

export const useTerminalStore = defineStore('counter', {
  state: () => ({
    settings: undefined as Settings | undefined
  }),
  actions: {
    async loadSettings (terminalId: string, terminalPassword: string): Promise<void> {
      try {
        // Show the loading indicator.
        Loading.show()

        // Fetch the settings from endpoint.
        const settingsResponse = await axios.get(`${process.env.API_URL}/terminal/${terminalId}/${terminalPassword}/settings`)

        // Assign the settings into our
        this.settings = settingsResponse.data as Settings
      } catch (err: any) {
        // Log the error to the console.
        console.log(err)

        // By default, display a negative error.
        let type = 'negative'
        let message = err.message

        if (err.response && err.response.status === 404) {
          type = 'info'
          message = 'Setting up Terminal for the first time...'
        }

        // And notify the user.
        Notify.create({ type, message })
      } finally {
        // Hide the loading indicator.
        Loading.hide()
      }
    },

    async saveSettings (terminalId: string, settings: Settings): Promise<void> {
      try {
        // Show the loading indicator.
        Loading.show()

        // Send a post request to save the settings.
        await axios.post(`${process.env.API_URL}/terminal/${terminalId}/settings`, settings)

        // Notify the user that the settings have been updated successfully.
        Notify.create({ type: 'positive', message: 'Settings saved' })

        // Load the updated settings.
        await this.loadSettings(terminalId, 'asd')
      } catch (err: any) {
        // Log the error to the console.
        console.log(err)

        // And notify the user.
        Notify.create({ type: 'negative', message: err.message })
      } finally {
        // Hide the loading indicator.
        Loading.hide()
      }
    }
  }
})
