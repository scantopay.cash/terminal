export interface Settings {
  memo: string;
  payoutAddress: string;
  currency: string;
  expiry: number;
  password: string;
}

export interface PendingPayment {
  status?: string
  invoice?: any
}

export interface PaymentRequest {
  amount: string
}
