import QRCodeStyling from 'qr-code-styling'
import { customAlphabet } from 'nanoid'
import { decodeCashAddress } from '@bitauth/libauth'
import currencies from './currencies'

export async function generateQrCode (paymentUrl: string): Promise<Blob> {
  const qrCode = new QRCodeStyling({
    width: 219,
    height: 219,
    type: 'svg',
    data: paymentUrl,
    image: '/bch.svg',
    imageOptions: {
      hideBackgroundDots: false
    },
    cornersDotOptions: {
      type: 'dot'
    },
    cornersSquareOptions: {
      type: 'extra-rounded'
    },
    dotsOptions: {
      color: '#000',
      type: 'extra-rounded'
    },
    backgroundOptions: {
      color: '#fff'
    }
  })

  const rawSVG = await qrCode.getRawData('svg')

  if (!rawSVG) {
    throw new Error('Failed to get raw data for SVG.')
  }

  return rawSVG
}

export function generateShortId (): string {
  const nanoid = customAlphabet('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', 8)
  return nanoid(8)
}

export function generatePassword (): string {
  const nanoid = customAlphabet('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ', 16)
  return nanoid(16)
}

export function isValidCashAddr (address: string): boolean {
  const addressIsCashAddr = (typeof decodeCashAddress(address) !== 'string')

  return addressIsCashAddr
}

export function getCurrencySymbol (currency: string | undefined) {
  if (!currency) return '$'
  return currencies[currency] || '$'
}

// We need to use this hacky function because JS Math is awful.
// In short, an example of the problem:
// 4.6 * 100_000_000 should yield 4.60000000
// but JS's Math engine yields 4.59999999.99999994
export function satsToBCH (satoshis: number): number {
  let satoshisAsString = satoshis.toString()

  if (satoshisAsString.length < 9) {
    satoshisAsString = satoshisAsString.padStart(9, '0')
  }

  const shiftPositionsForBCH = -8

  const beforeDecimal = satoshisAsString.slice(0, satoshisAsString.length + shiftPositionsForBCH)
  const afterDecimal = satoshisAsString.slice(shiftPositionsForBCH)

  const bchAsString = `${beforeDecimal}.${afterDecimal}`

  return Number(bchAsString)
}
