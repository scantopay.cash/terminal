**THIS CODE IS VERY POOR QUALITY. I DO NOT RECOMMEND ATTEMPTING TO BUILD ON-TOP OF IT. IT WILL BE SEVERELY REFACTORED FOR TIDINESS AND MAINTENABILITY IN FUTURE PENDING FEEDBACK ON VIABILITY OF THE GENERAL CONCEPT**


# PaperPay (paperpay)

A Quasar Project

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
